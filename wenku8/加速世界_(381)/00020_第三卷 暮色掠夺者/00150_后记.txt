各位讀者好久不見，也有部分讀者是初次見面，大家好，我是川原礫，非常感謝各位讀者看完《加速世界3暮色掠奪者》。

雖然加速世界這個系列是采「VR格闘遊戲小說」的形式，但嚴格說來並不能算是遊戲小說，相信早在第一集，應該就有很多讀者已經注意到這一點。

原因很簡單，因為有時候「氣勢」與「奇跡」這種難以捉摸的事物，往往會凌駕於書中所提出的系統之上。遊戲小說不能夠容許這樣的情形，而我也由衷希望可以避免這樣的情形出現（笑），但不知道為什麼，寫著寫著就是會寫成這樣。

這或許是因為長年來我心中一直有個揮之不去的疑問，那就是「遵照系統規則贏得的勝利，算得上真正的勝利嗎？」例如比剪刀石頭布，一邊出剪刀，另一邊出石頭贏得勝利，我就會懷疑這樣真的好嗎？會覺得說既然是主角（又或者是主角的勁敵），就應該要出布還能贏。

不，我自己也知道我在胡說八道！而我試圖將這種厘不清的疑問融入遊戲系統之中，構思出來的就是在本集之中登場的「心念系統」。內容就是讓想像力跟意志力轉變為遊戲中極為具體的勝敗因素，可說是一種為了掩飾胡說八道而存在的超胡說八道，至於走這樣的路線，今後這個故事是能繼續以遊戲小說的形式走下去，還是會陷入更為極端的混沌狀態……如果各位讀者願意一直看到自己的忍耐度容量用光為止，那就太令人欣慰了。

不過仔細想想就會覺得，就算在現實世界之中也是一樣，如果要說有什麼能量可以超出各式各樣的系統規範，也就是超出常識範疇，那唯一的答案應該也就只有想像力了。畢竟儘管在當今的社會裡不管怎麼闖，往往都會四處碰壁而令人只能嘆氣，但我們還是隨時可以透過想像來越過這些高牆。在這邊我就多寫一些這樣的大道理來打打迷糊仗吧。

每當有新人物登場，都要進行現實與虛擬兩方面的造型，真的給插畫家HIMA老師添了很多麻煩；此外責任編輯三木氏也為了讓照慣例消極得超凡入聖的春雪有點主角該有的樣子，投入莫大的心力，這次也真的多虧兩位大力相助了。

再來就是要對看完本書的您，送上憑我的心念所能具體化的全部感謝！

二零零九年七月二十三日  川原礫
