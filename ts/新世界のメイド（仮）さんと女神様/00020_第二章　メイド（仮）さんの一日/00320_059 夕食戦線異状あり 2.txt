恐怕，在看到這個狀況的時點，塔莉婭的頭腦裡就已經有好幾個方案了，只是在那之上需要先交給薩妮婭來判斷，麻理子這麼想。要說的話，這就是教育繼承人的一環。實際上，材料全部見底是不可能的，像在日本這邊餐廳的菜單就是是由手邊能用的食材所決定的。既然有了這幾點，今天剛來的麻理子都能想到的，塔莉婭沒有理由想不到。

然而，被委任的薩妮婭所採取的方法是讓現場擔當者的麻理子給出方案—— 用塔莉婭的說法就是把事情都拋給別人做。當然，這種悠悠閑閑的地方多少也是薩妮婭的性格使然。可是，在麻理子看來，自己的一切必須全部變成第一的「初代」老板娘塔莉婭與有能用的人就交給對方來辦的「二代目」老板娘薩妮婭之間不得不說是存在著一定的差距。

(也就是說，村子裡的情況發生了改變，老板娘所追求的東西也開始發生變化了嗎。但是，說話的樣子總是有點像塔莉婭桑呢)

「擱那嘿嘿的笑什麼呢，你」

心情十分欣慰的麻理子，將那些想法全都表現了臉上。

「誒？　不，沒什麼。只是在想，果然是母女啊」

「什。突然的這是說什麼」

麻理子將心裡想的話原原本本的說了出來後，塔莉婭罕見的把臉扭開了。

◇

在方針決定下來之後，麻理子被介紹給了後來的二人。紅茶色頭髮的姑娘自報姓名為席娜 ，水色頭髮的則是瑪琳 。這兩人身高同米蘭達差不多，不過胸部則是和茱莉亞同樣豐滿。只是，從體格上看的話席娜更加結實一些，瑪琳則稍微有些纖細。

初次見面，麻里子本想如此上前寒暄一下，但仔細觀察後卻發現對這二人有著印象。在昨天第一次來到旅店的時候同薩尼婭一起在櫃台裡站著的就是席娜，同亞里亞他們一起吃午餐的時候在旁邊負責服務的則是瑪琳。這兩個人，再加上伊莉、茱莉亞以及今天因為感冒而沒有來的繆拉一共五人，她們大體上都是全職在旅店工作的，另外據說在其他時間也會有幾人過來旅店幫忙。

就在三人相互打著招呼的時候，她們聽見了後面門打開的聲音。回頭看去發現米蘭達正在進來。

「啊， 米蘭達桑。那個，不要緊嗎？」

「 麻理子大人。啊啊，沒有問題」

對麻理子的詢問做出回答的米蘭達眼睛向旁邊游離著。臉頰也微妙的變得赤紅。

「沒問題麼，米蘭達桑，怎麼了？」

「總覺得，臉變得很紅」

「誒？　沒，沒有，沒有怎麼」

被席娜和瑪琳以一臉不可思議的表情問道的米蘭達慌張的將臉用手捂住。那個動作在二人看來倒不如說是肯定有著什麼。

「 啊啊 ， 誒都 ， 米蘭達桑在之前的那段空閑時間裡，與我稍微練了一會劍。大概，我想是那個的原因」

「哎？　 麻理子桑，已經被米蘭達桑挑戰了？」

「嘛。那對麻理子桑來說不是很嚴重的嗎？」

禁不住接上麻理子話的二人發出了同情的聲音。而這次米蘭達喊了出來。

「什麼叫很嚴重啊。 是麻理子大人贏了我」

「唉呀!?」

「是那樣嗎？」

「啊啊。手也好腳也好都沒能碰到。除了用大敗以外沒有可以形容的了」

「唔啊， 米蘭達桑大敗」

「哇」

這回二人以畏懼的眼神看向了麻理子 。感覺有些不舒服的麻理子搖了搖手想進行辯解。

「不，偶然啦，偶然的。只是運氣好而已」

「 麻理子大人。那個太過謙虛了。難道光偶然就能使出那麼多次的那種技能嗎」

「誒都……」

「你們，劍術的話到這裡為止。撒，開始準備了！」

剛起了一點小騷動在一旁觀察狀況的塔莉婭就發出聲音，大家暫且平息了下來。

（這個，之後絕對會再次騷動起來）

◇

給來晚的米蘭達進行了說明與簡單的商量之後，大家就各自分散到了自己的工作之中。茱莉亞和瑪琳負責給等待的客人進行說明和客席的準備， 塔莉婭和席娜是飯、蔬菜和湯的準備， 薩妮婭、麻理子和米蘭 則是負責準備油炸以及燒烤。 亞里亞帶著卡薩魯回到裡面。

返回了白天時工作崗位的麻理子她們，首先做的是取火。雖是那麼說，但白天時候的炭火還有剩，也有著煮米的火，所以只是加進足夠的柴火讓火變得更大而已。將那份工作托付給米蘭達的麻理子和薩妮婭 ，從冰箱中將中午那時準備好的材料排列在烹飪台上。

某種程度上，在進行油炸的途中能夠與烤雞肉串同時進行。 麻理子將袖口捲起充滿了幹勁。

（那麼，戰鬥從這裡就開始了啊）
