# novel

- title: 実は俺、最強でした？ ～転生直後はどん底スタート、でも万能魔法で逆転人生を上昇中！～
- title_zh: 其實，我乃最強
- author: すみもりさい
- illust: 高橋 愛
- source: http://ncode.syosetu.com/n1321ez/
- cover: https://pbs.twimg.com/media/D3DGbRJUgAAUQS0.jpg
- publisher: syosetu
- date: 2019-07-17T11:00:00+08:00
- status: 連載
- novel_status: 0x0300

## authors

- 澄守 彩

## illusts


## publishers

- syosetu

## series

- name: 実は俺、最強でした？ ～転生直後はどん底スタート、でも万能魔法で逆転人生を上昇中！～

## preface


```
在異世界轉生之後，我被認定為最弱，生命正處於危機之中。真悲哀。但是，在開始數日內就變成死亡結局了嗎！
但是我能使用的魔法只有一個。儘管如此，我為了生存，在研究上進行了反覆的研究——。

（´-`）.。oO（咦？又是我幹的嗎？）

總覺得這個魔法太萬能了！
難道我是最強的？
怎麼也不驕傲，我雖然弱但是謙虛踏實，用什麼都可以的萬能魔法來提升反轉人生!

【小説一巻発売！】
５月３１日（金）から小説版 第一巻が紙でも電子でも発売中！ 大幅加筆で楽しさ倍増。お義母さんも名前付きで出てくるよ！
ニコニコ静画 水曜日のシリウスにて公開中のコミカライズ版ともどもよろしくです！
－－－
　異世界転生した直後、最弱認定されて命がピンチな俺である。哀しい。が、開始数日でデッドエンドにしてなるものか！
　でも俺が使える魔法はひとつだけ。それでも俺は生きるため、研究に研究を重ねた結果――。
　
　（´-`）.。oO（あれ？　また俺なんかやっちゃったの？）
　
　なんだかこの魔法、万能すぎるぞ！
　もしかして俺って最強でした？
　なんてうぬぼれず、俺は弱いながらも謙虚・堅実に、何でもアリな万能魔法で逆転人生を昇っていく！
```

## tags

- node-novel
- R15
- syosetu
- ご都合アリ
- ざまぁもあるよ
- なんでもあり魔法
- コミカライズ化
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ファンタジー
- 主人公最強
- 勘違いコメディ
- 書籍化
- 残酷な描写あり
- 異世界転生
- 部下も強キャラ揃い
- 陰のヒーロー

# contribute

- player

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n1321ez

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n1321ez&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n1321ez/)
- 


