７

諾瑪是在十七歳那一年隨著父親離開瓦茲洛弗家，那是在王国歷百零七年。正好在十年前。
實在說不上，對瓦茲洛弗家有好印象。不如說，甚至抱著近似於憎恨的感情。

這個家，可是拆散了母親和父親，母親和諾瑪的家。不可能抱有好感。

此外，這個家是個冷漠的家。
父親和諾瑪被給予了好住所、好用品、好服裝、好食物，受優秀的傭人照料，父親為了研究所想要的還全都被給予了，但僅僅如此而已。

在宅邸裡，遇到父親的兄弟及其家族時，就算問候，也只會還以低頭或鞠躬，幾乎沒有人會對話。就連傭人們，也只會說最低限度的必要的話語。諾瑪和父親一直都被無視。宛如並不存在於那裡。

諾瑪從嬰兒到十七歳，都在這裡度過。多愁善感的時期的諾瑪，忍受不了那冷漠。所以很悲傷，憎恨著這個家。覺得這個家也憎恨著自己兩人。

但在去年，從雷肯那裡得知了意外的事實。

父親和諾瑪，其實都被愛著。經他這麼一說，確實如此。對諾瑪來說，金格正是能無條件相信的庇護者及理解者，是無可取代的人。將這金格送來的是瓦茲洛弗家。給予了如此之大的禮物。其中存在著強烈的愛情，這毫無懷疑之餘地。

一定會報弟弟的仇，每次想到前代侯爵葛德夫利的這句話，鼻子就會酸酸的，胸口就會發熱。葛德夫利對父親薩斯夫利這年齡相距甚多的弟弟，抱有這等思念。

「葛德夫利是侯爵這機械。但是，我覺得，在妳父親死去時的那話語，是流著熱血的人的話語」
「然後葛德夫利報了弟弟的仇，取回了杖，寄給金格」
「把妳母親的杖送到了妳身邊。這之中飽含著葛德夫利對妳的心意」

雷肯這個人，雖然平常的態度很冷淡，但擁有看清人之真情的力量。透過雷肯的那洞察力，諾瑪得以接觸到前代侯爵的心意。

如此一來，對瓦茲洛弗家的憎恨，便如雲一般消散了。仔細一想，在瓦茲洛弗家的待遇並不差。就連監禁母親這件事，也能認為是只有這樣能保護母親。

不，其實在最初就知道了。諾瑪的伶俐頭腦和觀察力，有好好把握到，自己兩人受到了瓦茲洛弗家的厚遇。但是，不想承認。

現在在眼前的曼夫利雖然是堂兄，但幾乎沒有談過話。但有個與此人相關的光景，現在也歷歷在目。十年前，諾瑪和父親離開這個家時，有個站在門旁一直看著這邊的曼夫利的身影。應該不可能特意出來目送，但想不出其他在那種場所的理由。那身影的記憶，成了寂寞之中的些許慰藉。

所以諾瑪並沒有對曼夫利抱持惡意。反倒是在這個家之中的不可思議又懷念的人。

「整理叔父大人的著作的作業，有在進展嗎？」
「是。最初是打算，盡可能把父親所留下的，在經過最低限度的修正後直接作為原稿。不過，得到助言並重新考慮後，決定將我所了解的，父親真正想留下的給創造出來，並再度執筆各個著作」
「喔。那不會很辛苦嗎？」
「不。鑽研已經存在的，並把其意思好好表現出來，會辛苦更多倍。無中生有寫出新物，既簡單又確實許多，而且，在最後注意到了，那才是父親想寫的」
「呼嗯。能問問是誰給予助言的嗎」
「是普拉多・工庫魯卿和抄寫師拉庫魯斯殿」
「拉庫魯斯殿嗎。是能說是王国目前最好的抄寫師的人。要委託叔父大人的著作，沒有更適合的人了。然後是普拉多殿嗎。妳在工庫魯家之中找到自己的居所了呢」
「是的」
「是嗎。太好了」

喝茶的那張臉，看起來浮現了些許笑容。

「話說回來，想再重新請求一次。在叔父大人的著作上，能將著者名記為薩斯夫利・瓦茲洛弗嗎。拜託了」
「好的。我才該如此請求。父親應該也會對此感到高興吧」
「願意同意嗎。太感激了。以前，王都艾雷克斯神殿的亞馬密爾一級神官大人曾拜訪過吾家。在那一天的晚宴上，父親的心情非常好。說了，為尋求薩斯夫利的著作特地參見了吾家，吾弟的研究似乎被那方面的專家視為第一級的研究，乾杯了好幾次」
「是。當時，葛德夫利大人似乎清楚地說了，那是弟弟薩斯夫利寫的。然後還說了，薩斯夫利雖然已死，但關於其他研究，沃卡城裡名為諾瑪的施療師就會知道吧」
「喔。原來如此。是這麼一回事嗎。啊啊，所以才。原來如此。不過，真是幫大忙了。要是叔父大人的著作上沒有添加瓦茲洛弗之名，會被亡父斥責的」

曼夫利以茶潤喉，改變了話題。

「那麼，來說說這次讓妳過來的原因吧」
「是」
「知道〈白雪花之姫〉嗎？」
「不。沒有聽過」
「是嗎。叔父大人將這保持為秘密過世了嗎」

曼夫利接著，講述了與諾瑪的出身有關的驚人話語。